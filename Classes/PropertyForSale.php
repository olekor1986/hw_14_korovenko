<?php


abstract class PropertyForSale
{
    protected $area;
    protected $house;
    protected $price;
    protected $type;
    protected $credit;

    public function __construct($area, $house, $price)
    {
        $this->area = $area;
        $this->price = $price;
        $this->house = $house;
        $this->type = static::getType();
        $this->credit = static::getCreditSummaryLine();
    }
    //получение наименования недвижимости
    public function getType()
    {
        return 'Недвижимость';
    }
    //получение площади
    public function getArea()
    {
        return $this->area;
    }
    //получение полной стоимости
    public function getFixedPrice()
    {
        return $this->price * $this->area;
    }
    //расчет первого платежа
    public function getFirstPayment()
    {
        return $this->area * 0.1 * $this->price;
    }
    //получение объекта класса House
    public function getHouse()
    {
        return $this->house;
    }
    //вычисляем количество месяцев до окончания строительства
    public function getRemainingBuildMonth()
    {
        $now_date = new DateTime('now');
        $house = $this->getHouse();
        $finish_date = $house->getFinishDate();
        $interval = $now_date->diff($finish_date);
        if ($house->getStatus() == 0) {
            return floor($interval->{'days'} / 30);
        } else {
            return "Дом сдан";
        }
    }
    //вычисляем ежемесчный платеж по кредиту
    public function getMonthlyPayment()
    {
        $house = $this->getHouse();
        if ($house->getStatus() == 0) {
            return round(($this->getFixedPrice() - $this->getFirstPayment()) / $this->getRemainingBuildMonth(), 1);
        } else {
            return "Дом сдан";
        }
    }
    //строка с данными по кредиту
    public function getCreditSummaryLine()
    {
        $house = $this->getHouse();
        if ($house->getStatus() == 0) {
            $credit = "Аванс - " . $this->getFirstPayment() . "$, ";
            $credit .= "Рассрочка на " . $this->getRemainingBuildMonth() ." мес., ";
            $credit .= "Ежемесячный платеж - " . $this->getMonthlyPayment() . "$.";
            return $credit;
        } else {
            return "Рассрочки нет.";
        }
    }
}