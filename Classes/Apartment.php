<?php


class Apartment extends PropertyForSale
{
    //получение типа недвижимости
    public function getType()
    {
        return "Апартаменты";
    }
    //результат только для класса Apartment
    public function getCreditSummaryLine()
    {
        return "Рассрочки нет.";
    }

}