<?php


class House
{
    protected $finish_date;

    public function __construct($finish_date)
    {
        $this->finish_date = $finish_date;
    }
    //получение даты окончания строительства
    public function getFinishDate()
    {
        return $this->finish_date;
    }
    //вычисляем, построен или нет объект недвижимости
    public function getStatus()
    {
        $now_date = new DateTime('now');
        $interval = $now_date->diff($this->finish_date);
        $result = $interval->format('%R%a');
        if ($result > 0) {
            return 0;
        } else {
            return 1;
        }
    }
}