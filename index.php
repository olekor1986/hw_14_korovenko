<?php
require_once "testArray.php";
require_once "Classes/PropertyForSale.php";
require_once "Classes/House.php";
require_once "Classes/Apartment.php";
require_once "Classes/Studio.php";
require_once "Classes/ParkingPlace.php";

//создаем несколько объектов класса DateTime
$finish_date_1 = new DateTime();
$finish_date_2 = new DateTime();
$finish_date_3 = new DateTime();

//устаналиваем для каждого произвольную дату
$finish_date_1->setDate(2020, 7, 01);
$finish_date_2->setDate(2021, 5, 30);
$finish_date_3->setDate(2019, 9, 15);

//создаем экземпляры класса House
$house_1 = new House($finish_date_1);
$house_2 = new House($finish_date_2);
$house_3 = new House($finish_date_3);

//создаем массив экземпляров классов Apartment, Studio, ParkingPlace
$propertyObjects = [];
foreach ($testArray as $value) {
    if ($value['type'] == 'apartment') {
        $propertyObjects[] = new Apartment($value['area'], $house_1, $value['price']);
    }
    if ($value['type'] == 'studio') {
        $propertyObjects[] = new Studio($value['area'], $house_2, $value['price']);
    }
    if ($value['type'] == 'parking_place') {
        $propertyObjects[] = new ParkingPlace($value['area'], $house_3, $value['price']);
    }
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Realty</title>
</head>
<body>
<h3>Недвижимость на продажу</h3>
<div>
    <ol>
        <?php foreach ($propertyObjects as $object):?>
            <li><?=$object->getType() . ". " . "Площадь - " . $object->getArea() ."м2,  " .
                "Общая стоимость - " . $object->getFixedPrice() . "$, " . $object->getCreditSummaryLine()?></li>
        <?php endforeach;?>
    </ol>
</div>
</body>
</html>