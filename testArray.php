<?php
$testArray = [
    [
        'type' => 'apartment',
        'area' => 115,
        'price' => 3500,
    ],
    [
        'type' => 'apartment',
        'area' => 165,
        'price' => 2500,
    ],
    [
        'type' => 'apartment',
        'area' => 180,
        'price' => 4500,
    ],
    [
        'type' => 'studio',
        'area' => 30,
        'price' => 2500,
    ],
    [
        'type' => 'studio',
        'area' => 27,
        'price' => 1800,
    ],
    [
        'type' => 'studio',
        'area' => 35,
        'price' => 2000,
    ],
    [
        'type' => 'parking_place',
        'area' => 8,
        'price' => 500,
    ],
    [
        'type' => 'parking_place',
        'area' => 12,
        'price' => 800,
    ],
    [
        'type' => 'parking_place',
        'area' => 10,
        'price' => 650,
    ]
];
?>